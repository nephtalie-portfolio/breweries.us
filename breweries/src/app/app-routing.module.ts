import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrewerieHomeComponent } from './brewerie-home/brewerie-home.component';
import { BrewerieListComponent } from './brewerie-list/brewerie-list.component';
import { BrewerieProfileComponent } from './brewerie-profile/brewerie-profile.component';

const routes: Routes = [
  {path:'' , component: BrewerieHomeComponent},
  {path:'breweries', component: BrewerieListComponent, children:[
      {path: ':id', component:BrewerieProfileComponent}
  ]},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
