import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BrewerieProfileComponent } from './brewerie-profile/brewerie-profile.component';
import { BrewerieListComponent } from './brewerie-list/brewerie-list.component';
import { BrewerieHomeComponent } from './brewerie-home/brewerie-home.component';
import { FooterComponent } from './footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { BarService } from './bar.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BrewerieProfileComponent,
    BrewerieListComponent,
    BrewerieHomeComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [BarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
