import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class BarService{
constructor(private http : HttpClient){}

    fetchBars():Observable<Bar[]>{
    return this.http.get<Bar[]>('https://api.openbrewerydb.org/breweries');
    }

    fetchBar(id:number):Observable<Bar>{
    return this.http.get<Bar>("https://api.openbrewerydb.org/breweries/"+id);
    }
}