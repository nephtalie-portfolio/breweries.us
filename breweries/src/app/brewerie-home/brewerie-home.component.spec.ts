import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrewerieHomeComponent } from './brewerie-home.component';

describe('BrewerieHomeComponent', () => {
  let component: BrewerieHomeComponent;
  let fixture: ComponentFixture<BrewerieHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrewerieHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrewerieHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
