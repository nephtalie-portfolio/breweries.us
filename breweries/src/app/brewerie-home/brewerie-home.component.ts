import { Component, OnInit, OnDestroy } from '@angular/core';
import { BarService } from '../bar.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-brewerie-home',
  templateUrl: './brewerie-home.component.html',
  styleUrls: ['./brewerie-home.component.css']
})
export class BrewerieHomeComponent implements OnInit, OnDestroy{
  
barSubscrition = new Subscription();

  constructor(private barService:BarService, private router:Router, private route: ActivatedRoute) { }

  ngOnInit() {
  }


  onFeelingLucky(){
    this.barSubscrition = this.barService.fetchBars().subscribe(
      bars => {
        let barIds: number[] = []; 
        for(let b of bars){
          barIds.push(b.id);
     
          
        }
        this.getLuckyBar(barIds);
        
      }
    );

  }

  // Passed a random bar id to the route 
  getLuckyBar(ids:number[]){
    let luckyNumber = Math.floor(Math.random()*20);
    let luckyBar =ids[luckyNumber];
    this.router.navigate(['breweries/'+luckyBar], { relativeTo: this.route})
  }



ngOnDestroy(): void {
  this.barSubscrition.unsubscribe();
  }
}
