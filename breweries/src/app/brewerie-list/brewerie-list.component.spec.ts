import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrewerieListComponent } from './brewerie-list.component';

describe('BrewerieListComponent', () => {
  let component: BrewerieListComponent;
  let fixture: ComponentFixture<BrewerieListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrewerieListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrewerieListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
