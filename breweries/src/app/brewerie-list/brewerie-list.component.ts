import { Component, OnInit, OnDestroy } from '@angular/core';
import { BarService } from '../bar.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-brewerie-list',
  templateUrl: './brewerie-list.component.html',
  styleUrls: ['./brewerie-list.component.css']
})
export class BrewerieListComponent implements OnInit, OnDestroy {
 
barlist: Bar[] = [];
barSubscription = new Subscription();

  constructor(private barService: BarService) { }

  ngOnInit() {

    this.barService.fetchBars().subscribe(
      barList =>{
        
        this.barlist = barList;
      }
    )
  }

  ngOnDestroy(): void {
    this.barSubscription.unsubscribe();
  }

}
