import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrewerieProfileComponent } from './brewerie-profile.component';

describe('BrewerieProfileComponent', () => {
  let component: BrewerieProfileComponent;
  let fixture: ComponentFixture<BrewerieProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrewerieProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrewerieProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
