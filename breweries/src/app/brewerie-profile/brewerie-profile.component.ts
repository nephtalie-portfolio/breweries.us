import { Component, OnInit, OnDestroy } from '@angular/core';
import { BarService } from '../bar.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-brewerie-profile',
  templateUrl: './brewerie-profile.component.html',
  styleUrls: ['./brewerie-profile.component.css']
})
export class BrewerieProfileComponent implements OnInit, OnDestroy {
bar:Bar={
  "id": null,
  "name": null
};
barSubscription = new Subscription();

  constructor(private barService : BarService, private route :ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.barSubscription = this.barService
    .fetchBar(+params['id'])
      .subscribe( bar => {
      this.bar = bar;
    }
    );
    })

    
  }

 ngOnDestroy(): void {
    this.barSubscription.unsubscribe();
  }

}
